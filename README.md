
### Documentation
https://spring.io/guides
RestController : https://spring.io/guides/gs/rest-service/
Baeldung

### Description des dépendances

Springframework.boot : 
Propose des starters contenant des paquets de dépendances

spring-boot-devtools :
- permet de recompiler sans redemarrer le serveur
- scope = runtime --> cette dépendance n'apparaît pas dans le jar final, seulement au runtime

Base h2 :	
- Base en mémoire
- utilisé essentiellement pour du dev (poc, tests, ...)
- évite de choisir une base en particulir et d'avoir à tout installer

spring-boot-starter-data-jpa:
contient toutes les dépendances pour faire du jpa

spring-boot-maven-plugin
- permet d'exécuter directement sans déploiement sur le serveur web
- aide à créer un fichier jar qui est exécutable

spring-boot-starter-test
utilisé pour les tests

Jackson : 
librairie populaire pour transformer java en json et vice versa

starter-web
permet de faire des webservices

### Commande lancement
mvn spring-boot:run

### Annotations
 
 @SpringBootApplication :
 portée par la classe contenant le main 
 
 @Entity :
- permet de serialiser un objet

 @Id :
 Identifiant technique de l'objet (unicité)
 
 @OneToMany(mappedBy="mangeur") :
 permet de lier un objet à une liste d'objets
 possibilité de mettre la lisiaosn dans les deux objets en mettant un @ManyToOne sur l'autre objet
 
 @Repository
 classe permettant de faire des actions sur la db
 la classe CrudRepository<Mangeur, String> qu'elle étend propose les méthod CRUD (create, update, delete)
 
 @RestController :
 - contient la logique concernant les actions effectuées par l'utilisateur
 - à mettre sur la classe qui intercepte les requêtes
 - utilise REST (REpresentational State Transfer) --> style d'architecture logicielle définissant un ensemble de contraintes à utiliser pour créer des services web.
 
 @GetMapping("/mangeur") :
 à mettre devant la methode de récupération de données
 
 @PostMapping(path = "/mangeur", consumes = "application/json", produces = "application/json") :
 à mettre devant la methode de création/mise à jour de données
 
 @DeleteMapping(value = "/supprimer/{prenom}") :
 à mettre devant la methode de suppression de données
 
 ResponseEntity :
 peut être utilisé en retour d'un @DeleteMapping par exemple
 contient un statut (200, ...), des headers et un body 
 
#### RAPPEL VERBES HTTP

GET :
Permet de récupérer les données d’une ou plusieurs entités dans une ressource. Il est possible de filtrer par des paramètres dans l’url.

POST :
Permet de rajouter des entités à une ressource. Les données sont envoyées dans le corps de la requête.

PUT :
Permet de modifier les données d’une entité dans une ressource.
L’url doit indiquer l’identifiant de l’entité. Si l’identifiant est inexistant, l’entité devrait être créée.

DELETE :
Permet de supprimer une entité dans une ressource.

PATCH (OPTIONNEL) :
Permet de modifier en partie les données d’une entité dans une ressource.
 
### Creation DB

#### pré-requis

Avoir installé la base de données postgresql en version 11.2 et avoir l'utilisateur admin.
 
#### création de la base de données
```  
<répertoire_postgresql>\bin\createdb.exe -U admin -E UTF8 mmsdb
```
Exemple :
```
\Poste_AliceBackend\postgresql-11.2\bin\createdb.exe -U admin -E UTF8 mmsdb
```

#### création du user mms pour la base de données
```  
<répertoire_postgresql>\bin\bin\psql.exe -U admin -d mmsdb -c "CREATE USER mms WITH PASSWORD 'mms'"
```
Exemple :
```
\Poste_AliceBackend\postgresql-11.2\bin\psql.exe -U admin -d mmsdb -c "CREATE USER mms WITH PASSWORD 'mms'"
```

#### création du user mms pour la base de données
```  
<répertoire_postgresql>\bin\bin\psql.exe -U admin -d mmsdb -c "CREATE SCHEMA IF NOT EXISTS mms AUTHORIZATION mms"
```
Exemple :
```
\Poste_AliceBackend\postgresql-11.2\bin\psql.exe -U admin -d mmsdb -c "CREATE SCHEMA IF NOT EXISTS mms AUTHORIZATION mms"
```

#### Infos connexion 
url= jdbc:postgresql://localhost:5432/mmsdb
username=mms
password=mms
 