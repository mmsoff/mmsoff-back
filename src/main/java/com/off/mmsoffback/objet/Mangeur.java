
package com.off.mmsoffback.objet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Mangeur implements Serializable {

    private static final long serialVersionUID = 2443247666112037998L;

    private String prenom = "";

    @Id
    private String nom = "";
    private boolean estRadin = false;
    @OneToMany(mappedBy = "mangeur", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Bonbon> listeBonbons = new ArrayList<>();

    public Mangeur() {

    }

    public Mangeur(final String prenom, final String nom, final boolean estRadin) {
        this.prenom = prenom;
        this.nom = nom;
        this.estRadin = estRadin;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(final String nom) {
        this.nom = nom;
    }

    public boolean isEstRadin() {
        return estRadin;
    }

    public void setEstRadin(final boolean estRadin) {
        this.estRadin = estRadin;
    }

    public List<Bonbon> getListeBonbons() {
        return listeBonbons;
    }

    @Override
    public String toString() {
        return "Mangeur [prenom=" + prenom + ", nom=" + nom + ", estRadin=" + estRadin + ", listeBonbons="
                + listeBonbons + "]";
    }

}
