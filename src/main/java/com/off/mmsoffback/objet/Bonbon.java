
package com.off.mmsoffback.objet;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Bonbon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String gtin = "";
    private String marque = "";
    @ManyToOne
    @JsonIgnore
    private Mangeur mangeur;

    private Bonbon() {
        // Constreur pour JPA
    }

    /**
     * Constructeur de bonbon pour gourmand.
     *
     * @param gtin
     * @param marque
     */
    public Bonbon(final String gtin, final String marque) {
        this.gtin = gtin;
        this.marque = marque;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(final String gtin) {
        this.gtin = gtin;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(final String marque) {
        this.marque = marque;
    }

    public Mangeur getMangeur() {
        return mangeur;
    }

    public void setMangeur(final Mangeur mangeur) {
        this.mangeur = mangeur;
    }

    @Override
    public String toString() {
        return "Bonbon [gtin=" + gtin + ", marque=" + marque + "]";
    }

}
