package com.off.mmsoffback.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.off.mmsoffback.objet.Mangeur;

@Repository
public interface MangeurRepository extends CrudRepository<Mangeur, String> {

	public List<Mangeur> findByPrenom(String prenom);
	
}
