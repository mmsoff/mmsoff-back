package com.off.mmsoffback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmsoffBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmsoffBackApplication.class, args);
	}

}
