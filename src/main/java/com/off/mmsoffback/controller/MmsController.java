
package com.off.mmsoffback.controller;

import com.off.mmsoffback.Repository.MangeurRepository;
import com.off.mmsoffback.objet.Bonbon;
import com.off.mmsoffback.objet.Mangeur;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

/**
 * RestController doc : https://spring.io/guides/gs/rest-service/
 */
@RestController
public class MmsController {

    Logger logger = LoggerFactory.getLogger(MmsController.class);

    @Autowired
    MangeurRepository mangeureRepo;

    @GetMapping("/mangeur")
    public ResponseEntity<Mangeur> mangeur(@RequestParam(required = false, value = "nom") final String nom) {
        final Optional<Mangeur> mangeurOptional = mangeureRepo.findById(nom);
        if (mangeurOptional.isPresent()) {
            final Mangeur mangeur = mangeurOptional.get();
            logger.debug("On a trouvé " + mangeur.getPrenom() + " !");
            return new ResponseEntity<>(mangeur, HttpStatus.OK);
        }
        logger.error("On n'a rien trouvé :( !");

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/mangeurs")
    public List<Mangeur> mangeurs() {
        final Iterable<Mangeur> mangeurIterable = mangeureRepo.findAll();
        final List<Mangeur> listeMangeurs = new ArrayList<>();
        mangeurIterable.forEach(listeMangeurs::add);
        return listeMangeurs;
    }

    @PostMapping(path = "/mangeur", consumes = "application/json", produces = "application/json")
    public void addMangeur(@RequestBody final Mangeur mangeur) {
        mangeureRepo.save(mangeur);
    }

    @DeleteMapping(value = "/supprimer/{prenom}")
    public ResponseEntity<String> deletePost(@PathVariable final String prenom) {
        final List<Mangeur> mangeurs = mangeureRepo.findByPrenom(prenom);
        mangeureRepo.deleteAll(mangeurs);
        return new ResponseEntity<>(prenom, HttpStatus.OK);
    }

    @PostMapping(path = "/ajouter/bonbon")
    @Transactional
    public void ajouterBonbonAMangeur(@RequestParam(required = true, value = "gtin") final String gtin,
            @RequestParam(required = true, value = "marque") final String marque,
            @RequestParam(required = true, value = "nom") final String nom) {
        final Optional<Mangeur> mangeurOpt = mangeureRepo.findById(nom);
        if (mangeurOpt.isPresent()) {
            final Mangeur mangeur = mangeurOpt.get();
            final Bonbon bonbonCree = new Bonbon(gtin, marque);
            bonbonCree.setMangeur(mangeur);
            mangeur.getListeBonbons().add(bonbonCree);
            mangeureRepo.save(mangeur);
        }
    }
}
