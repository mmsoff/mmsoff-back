/*DROP TABLE IF EXISTS mangeur;

CREATE TABLE mangeur (
  prenom VARCHAR(50) NOT NULL,
  nom VARCHAR(50) NOT NULL PRIMARY KEY,
  est_radin boolean DEFAULT false
);*/

INSERT INTO mangeur (prenom, nom, est_radin) VALUES
  ('Marco', 'COLIN', false),
  ('Joel', 'UNG', true),
  ('Isabelle', 'SALA', false),
  ('Pauline', 'GALVAO', true);